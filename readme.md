# Readme

## Downloading sourcetree

Go to the SourceTree website (www.sourcetree.org)
Download SourceTree.
Start SourceTree up.

## Source clone repo

Go to the repo on BitBucket (https://bitbucket.org/Pobadger/program-jb)
Get URL of repo from top right hand corner.
Open SourceTree (if you haven't already)
Paste it into SourceTree repo
Click on the new repository button.
A drop down menu will appear
Select clone from url.
Paste the Url into the source url field, the other fields will be automatically filled in (you can change these if you wish.)



## Node set up

First you need to install node.js

```sh
npm install -g http-server
npm install -g stubby
```

## Use npm

```sh
cd /path/to/project/folder/
npm install
```

## Start Servers

```sh
http-server
stubby --watch --data mocks/auth-stubby.yaml
```

## Open website

Open browser and browse to: `http://localhost:8080`

## using github/source tree repo.

sourcetree,commit files to store changes and store old working versions of the project.
